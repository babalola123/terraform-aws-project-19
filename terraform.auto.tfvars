region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

max_subnets = 20

name = "project19"

# ami = "ami-0574da719dca65348"

ami-web = "ami-089eb9203323603e1"

ami-bastion = "ami-0342ac93e3c051c99"

ami-nginx = "ami-0e7a0dd2c60fe3bdc"

ami-sonar = "ami-06bb2c7aaf2de12bf"

keypair = "PBL-Devops-baba"

#Ensure to change this to your acccount number

account_no = "228341882219"

master-username = "ACSadmin"

master-password = "password12345"

tags = {
  Enviroment      = "production"
  Owner-Email     = "babaloladeen1@gnail.com"
  Managed-By      = "Terraform"
  Billing-Account = "xxxxxxxxxx"

}