# terraform {
#   backend "s3" {
#     bucket         = "babadeen-terraform-bucket"
#     key            = "global/s3/terraform.tfstate"
#     region         = "us-east-1"
#     dynamodb_table = "terraform-locks"
#     encrypt        = true
#   }
# }


terraform {
  backend "remote" {
    organization = "babadeen-link"

    workspaces {
      name = "terraform-aws-pbl-19"
    }
  }
}